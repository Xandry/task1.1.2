package jm.task.core.jdbc;

import jm.task.core.jdbc.model.User;
import jm.task.core.jdbc.service.UserService;
import jm.task.core.jdbc.service.UserServiceImpl;

public class Main {
    public static void main(String[] args) {
        UserService userService = new UserServiceImpl();
        userService.createUsersTable();

        userService.saveUser("Charlie", "White", (byte) 43);
        userService.saveUser("Jack", "Martinez", (byte) 12);
        userService.saveUser("Billie", "Garcia", (byte) 87);
        userService.saveUser("Fred", "Hudson", (byte) 36);

        for (User user : userService.getAllUsers()) {
            System.out.println(user);
        }

        userService.cleanUsersTable();
        userService.dropUsersTable();
    }
}
